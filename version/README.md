本项目分两个版本：``egg-admin-pro``、``egg-admin-template``，具体请看下文。

## 预览地址
?> [egg-admin-pro 预览地址](https://github.eggadmin.com/egg-admin-pro/dist)、
[egg-admin-template 预览地址](https://github.eggadmin.com/egg-admin-template/dist)

## 功能点

?> 功能点说明：当前按```egg-admin-pro v1.0```总架构图来区分，有一点很明确，```Pro版```后续会增加更多丰富功能。

**简称说明**：[egg-admin-pro](https://github.eggadmin.com/egg-admin-pro/dist) 以下简称```Pro```，
[egg-admin-template](https://github.eggadmin.com/egg-admin-template/dist) 以下简称```Template``` 😀 代表包含、😂 代表不包含

| 功能点      | Pro   | Template    |   差异说明     |
| -----------| ----- | :----:    |               |
| 框架基础     |😀|😀| 指：``vite``、``vue``、``vue-router``、``vuex``等基础功能，包含```多端适配```
| api(接口封装)|😀|😀|
| icons(svg目录)|😀|😀|
| layouts(布局)|😀|😀|
| plugins-插件/自定义指令(``EAuth鉴权指令``、``EResize监听dom改变指令``、``EUtils全局工具函数``、``svg-icon组件``)|😀|😀|
| setting(系统配置)|😀|😀|
| styles(样式)|😀|😀|
| utils(工具函数)|😀|😀|
| components(组件)|😀|😂| Pro专供
| view(页面) - component-组件示例(``多个组件使用示例``)|😀|😂|Pro专供
|  view(页面) - dashboard-报表(``主控台``、``分析页``)|😀|😂|Pro专供
|  view(页面) - detail-详情(``高级``、``基础``)|😀|😂|Pro专供
|  view(页面) - editor-编辑器(``ckEditor5 富文本编辑器``、``simpleMde md文档编辑器``)|😀|😂|Pro专供
|  view(页面) - error-page错误页面(``403、404、500``)|😀|😂|Pro专供
|  view(页面) - form-表单(``高级``、``基础``、``表单详情``、``步进表单``)|😀|😂|Pro专供
|  view(页面) - list-列表(``基础``、``卡片``、``商品``、``新增编辑详情弹窗``)|😀|😂|Pro专供
|  view(页面) - login-登录(``登录``、``注册``)|😀|😂|Pro专供
|  view(页面) - method-方法(``复制到剪切板``、``时间日期操作``)|😀|😂|Pro专供
|  view(页面) - result-结果(``成功``、``失败``)|😀|😂|Pro专供
|  view(页面) - setting-设置(个人设置: ``社交账号``、``基本设置``、``消息通知``、``修改密码``、``安全设置``、个人中心：``文章``、``朋友``、``摄影作品``、``项目``)|😀|😂|Pro专供
|  view(页面) - system-系统管理(``菜单权限管理``、``角色权限管理``、``用户管理``)|😀|😂|Pro专供
|  view(页面) - tool-工具(``iframe-内嵌网页``、``log-日志``、``page-cache-页面缓存``)|😀|😂|Pro专供

## 商用说明

| 版本      |  是否开源|  交付方式  |  升级维护 |  技术支持   | 备注
| ---------| ------- | -------   | -----   | ------ |---
| Pro     |```￥399```付费商业授权 ``微信、支付宝``|``邮件``| 保持更新，逐渐丰富功能|加入付费内部群 ``优先回复`` ``不仅限框架问题``|不支持发票，一个自然主体(个人、公司)授权一次即可
| Template|开源免费 可商业|``gitee``、``github``| bug更新，看情况增加新功能 |免费交流群 ``不定期回复``|


## 开源项目为何很难坚持

开源是美丽而危险的。美丽是因为可以让很多人为了一个目标而努力，危险是因为你需要有持之以恒、乐于分享的精神。每天都会有大量的开源项目被创造，也有大量的开源项目死亡。

- ``技术具备一定的前瞻性和先进性``

学习钻研这些新技术，``花费大量业余休息时间``，``踩过很多坑``才有一些成果。

- ``保持生机保持更新``

事实上，作者有自己的工作要做，维护开源项目``需要大量时间和精力``，保持迭代保持更新需要很高的成本。

- ``文档友好并保证代码质量``

需要一个友好详细的文档，并且``代码质量要保证``，``注释要详细``，``方便二次开发``，``文档更新跟上``代码的更新，版本的迭代。

!> 综合以上，你会发现，长期坚持下去有难度，没有动力，就很难长久，即使作者有耐心热度，也会随着时间慢慢消耗殆尽.
