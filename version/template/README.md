template版是基于Pro版精简的版本，适合作为基础框架来开发项目，极少代码冗余，内置一些常用的工具。

# 预览地址
?> [egg-admin-template 预览地址](https://github.eggadmin.com/egg-admin-template/dist)

## 进度安排
项目接下来的新功能和版本更新安排，会在这里列出。

### 待安排

## 版本记录
版本发布记录，bug尽快更新。

### v1.0.0
第一版，获取方式 [gitee](https://gitee.com/zengxiaohui2019/egg-admin-template) 、[github](https://github.com/zengxiaohui2019/egg-admin-template)。

### v1.1.0
升级`vue` `element-plus` `vite`等为最新版本

| 名称        | 原版  | 新版     | 备注
| -----------| ----- | ----    |               |
| vue        | 3.0.11 | 3.2.26 |               |
| element-plus| 1.0.2-beta.40 | 1.3.0-beta.1 |  官方改动了`组件配置项size参数` 组件：`menu 导航栏` `icon` `scrollbar 滚动条`            |
| vue-router| 4.0.3 | 4.0.12 |               |
| @vitejs/plugin-vue| 1.1.5 | 1.10.2 |               |
| @vue/compiler-sfc| 3.0.7 | 3.2.26 |               |
| eslint-plugin-vue| 7.10.0 | 7.20.0 |               |
| vite-plugin-svg-icons| 0.4.3 | 0.7.1 |               |
| @element-plus/icons-vue|  | 0.2.4 |    新增的icon组件 默认已经全局安装使用时直接`<el-icon><alarm-clock /></el-icon>`         |

本次升级项目里面的变更文件详情：

| 目录       | 主要变更内容
| ---------     |               |
| `src/layouts/base-layout`      | `适配el-icon新的使用方式` `el-menu导航栏的适配` `el-scrollbar滚动条的适配`     |
| `src/main.js`      | `全局安装了@element-plus/icons-vue`     |

### v1.2.0
- 修复部分icon显示不正确的问题
- 修复开发环境下 提示`[@vue/compiler-sfc] ::v-deep...`
- 修复打包时 控制台打印 `warning: "@charset" must be the first rule in the file`

本次升级项目里面的变更文件详情：

| 目录       | 主要变更内容
| ---------     |               |
| `src/layouts/base-layout` `src/styles/layout/base-layout/layout.scss`     | `修复部分icon显示不正确的问题`     |
| `src/views/login` `src/icons/other`      | `修复更多登录不显示图标的问题`     |
| `vite.config.js`      | `修复打包时 控制台打印 warning: "@charset" `     |
