版本发布记录，bug尽快更新，新功能继续增加。

## v1.0.0
第一版，如已授权过请联系作者微信: ``badiweier`` 发你邮箱，未授权用户请查看 [egg-admin-pro 获取方式](version/README?id=商用说明)。

### v1.1.0
升级`vue` `element-plus` `vite`等为最新版本

| 名称        | 原版  | 新版     | 备注
| -----------| ----- | ----    |               |
| vue        | 3.0.11 | 3.2.26 |               |
| element-plus| 1.0.2-beta.40 | 1.3.0-beta.1 |  官方改动了`组件配置项size参数` 组件：`menu 导航栏` `icon` `scrollbar 滚动条`            |
| vue-router| 4.0.3 | 4.0.12 |               |
| @vitejs/plugin-vue| 1.1.5 | 1.10.2 |               |
| @vue/compiler-sfc| 3.0.7 | 3.2.26 |               |
| eslint-plugin-vue| 7.10.0 | 7.20.0 |               |
| vite-plugin-svg-icons| 0.4.3 | 0.7.1 |               |
| @element-plus/icons-vue|  | 0.2.4 |    新增的icon组件 默认已经全局安装使用时直接`<el-icon><alarm-clock /></el-icon>`         |

本次升级项目里面的变更文件详情：

| 目录       | 主要变更内容
| ---------     |               |
| `src/layouts/base-layout`      | `适配el-icon新的使用方式` `el-menu导航栏的适配` `el-scrollbar滚动条的适配`     |
| `src/main.js`      | `全局安装了@element-plus/icons-vue`     |
| `src/views/...vue`      | `适配el-icon新的使用方式`     |

### v1.2.0
- 修复部分icon显示不正确的问题
- 新增系统设置->角色权限管理->设置权限弹窗 tree结构高亮异常的问题
- 修复开发环境下 提示`[@vue/compiler-sfc] ::v-deep...`
- 修复打包时 控制台打印 `warning: "@charset" must be the first rule in the file`


本次升级项目里面的变更文件详情：

| 目录       | 主要变更内容
| ---------     |               |
| `src/layouts/base-layout` `src/styles/layout/base-layout/layout.scss`     | `修复开发环境下 提示[@vue/compiler-sfc] ::v-deep...`     |
| `src/views`   | `修复icon不正确的问题`     |
| `vite.config.js`      | `修复打包时 控制台打印 warning: "@charset" `     |
