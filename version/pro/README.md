一次付费，即刻拥有一个 ``高颜值`` 、 ``高性能`` 的企业级后台前端框架，内置多个典型业务模型，提供多个常用工具函数，提高开发效率。

## 不仅仅收获代码
代码不是交付了就完事，如果对着文档还是搞不定，可以联系作者协助远程支持，一共 ``5次远程协助``，如果有特别需求另说。

一般来说，对着文档基本上都没问题，代码注释非常详细，精细到每一个参数变量都给予说明。

举个栗子看看：
````js
// @/utils/index.js 局部
/**
 * 判断时间是否为空 不为空返回时间到分 为空返回-- 或自定义字符
 * @param date {*} 需要判断的时间字符串或者规范的时间格式 format 参考dayjs
 * @param str {string} 如果为空 返回自定义字符串 默认'--'
 * @return {string}
 */
export const isDate = (date, str = '--') => {
  if (isBlank(date)) {
    return str
  } 
  return _dayjs(date).format('YYYY-MM-DD HH:mm')
}

/**
 * 把dayjs暴露出去 满足不同需求
 * @return {*}
 */
export const dayjs = _dayjs
````

````js
// main.js 局部

// 全局样式
import '@/styles/index.scss'

// 全局工具函数插件
import EUtils from '@/plugins/EUtils'
// 权限校验插件
import EAuth from '@/plugins/EAuth'
// SvgIcon 插件
import SvgIcon from '@/plugins/SvgIcon'
// EResize 插件
import EResize from '@/plugins/EResize'

// Vue3CountTo 组件
import Vue3CountTo from '@/components/Vue3CountTo'
// Echart 组件
import Echart from '@/components/Echart/index.vue'
// Br16Div 组件
import Br16Div from '@/components/Br16Div/index.vue'
// ShowMore 显示更多组件
import ShowMore from '@/components/ShowMore/index.vue'

````

## 更收获一个交流圈
一次授权，融于一个新的圈子，内部交流群里有各行业地区的 ``前后端大佬`` ，一起讨论问题，唠唠嗑都是阔以的，结交更多新朋友，升职加薪机会也许就有了😄。

## 联系作者

加微信：``badiweier`` 

![微信](../../_media/weixin.jpg ':size=300px')
