接触一个新项目一门新技术，即使身经百战也难免会遇到一些小问题，下面收集了一些常见问题，供你查询：

?> 提问之前，请先查阅下面的常见问题。

## `esbuild.exe` 安装失败
作者是被360安全卫士拦截了，添加信任或退出360和其他杀毒软件再安装

## 设置别名后 如何让编辑器可点击跳转
webstorm：设置->语言和框架->JavaScript->Webpack->手动->选择根目录下的webstorm.config.js 其他编辑器自行百度

## 部分新特性不支持
- `Element.scrollTo()` tabs多页签用了，[兼容性参考](https://developer.mozilla.org/zh-CN/docs/Web/API/Element/scrollTo)
- `ResizeObserver` v-resize指令用了，[兼容性参考](https://developer.mozilla.org/zh-CN/docs/Web/API/ResizeObserver)
