### 快速开始

?> 假定你的电脑已经安装：[git](https://git-scm.com/)   [nodejs](https://nodejs.org/en/download/)， 如果未安装，点击链接安装就是了，``nodejs是必须`` git没有也无妨，假定你已经具备一定的前端脚手架知识，并了解vue。

#### 开发环境说明

为避免开发环境或依赖库不一致带来莫名其妙的错误，特说明以下。

- 操作系统：不限，作者用的是``mac``
- nodejs：>= 12，作者用的是``v14.16.1`` 查看版本方式：控制台输入``node -v``
- 浏览器：新版现代浏览器，最好是Chromium内核，比如：``Edge`` ``Chrome``  作者用的是``Edge``最新版

#### 安装项目

!> 获取Pro版 详见[商用说明](version/README?id=商用说明)

- 下载项目 (未安装git的请从代码仓库上打包后下载)
````shell
    git https://gitee.com/zengxiaohui2019/egg-admin-template.git
````
- 进入项目根目录，安装依赖 NPM/YARN (作者用的是npm)
````shell
    cd 你的文件夹/egg-admin-template
    npm i
````

#### 运行项目
````shell
    npm run dev
````
当控制台 出现  ```> Local: http://localhost:4000/``` 说明运行成功，点击地址或复制到浏览器地址栏，进行访问

?> 项目登录使用的是在线mock [fastmock](https://www.fastmock.site/)，请确保联网。

#### 打包项目
````shell
    npm run build
````

### 项目配置
<div xian></div>

?> 非特别说明，以下操作都在(src核心目录下)

#### 系统配置

系统配置文件 ``setting/index.js``

##### 配置项列表
| 属性      |  类型   |  默认值               |  必填    | 描述 
| ---------| ------- | -----                | ------- | ------- 
| sysTitle |  String |  Egg-Admin-Pro       | 是      |  系统名称全称和页面标题的后缀
| logoTitle |  String |  Egg Admin Pro      | 是      |  系统名称简写 左上角logo地方展示
| indexPage |  String |  /                  | 是      |  默认首页路由地址
| loginPage |  String |  login              | 是      |  默认登录页路由地址
| routerMode |  String |  hash              | 是      |  路由模式，可选值为 ``history`` 或 ``hash``
| tokenKey   |  String |  egg_admin_token   | 是      |  默认cookie的key参数，这个是为了将来需要使用cookie而设计，当前版本暂未用到
| layout   |  Object |  详见[layout配置项列表](#layout配置项列表)   | 是      |  布局配置

##### layout配置项列表
| 属性               |  类型   |  默认值               |  必填    | 描述
| ---------         | ------- | -----                | ------- | ------- 
| themeType         |  String |  light       | 是      |  主题风格配置 ``dark`` (绅士黑) 或者 ``light`` (极简白)
| logoShow          |  Boolean|  true        | 是      |  是否显示logo
| uniqueOpened      |  Boolean|  true        | 是      |  左侧菜单是否只保持一个子菜单的展开 默认true
| headerMaxWidth    |  Boolean|  false       | 是      |  顶部是否通顶（最大宽度）
| headerCollapseShow|  Boolean|  true        | 是      |  显示收缩菜单
| headerReloadShow  |  Boolean|  true        | 是      |  显示刷新
| headerBreadcrumbShow|Boolean|  true        | 是      |  显示面包屑导航
| headerFullscreenShow|Boolean|  true        | 是      |  显示全屏
| headerNoticeShow  |  Boolean|  true        | 是      |  显示通知
| headerUserShow    |  Boolean|  true        | 是      |  显示用户区域
| headerSettingShow |  Boolean|  true        | 是      |  显示设置
| tabsShow          |  Boolean|  true        | 是      |  显示多页签
| tabsReload        |  Boolean|  true        | 是      |  开启多页签重复点击重载页面
| copyrightShow     |  Boolean|  true        | 是      |  显示底部版权
