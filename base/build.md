## 构建
当项目开发完毕，只需要一行命令就可以执行应用的构建，默认情况下，构建会输出到 ``dist`` 文件夹中。

````shell
npm run build
````

## 本地测试应用
有的时候我们需要知道刚刚构建的应用是否正常，你可以运行 ``npm run preview`` 命令，在本地测试该应用。

````shell
npm run preview
````
?> 注意，先构建应用 ``build`` 后才可以执行本地测试应用 ``preview`` 。

``preview`` 命令会在本地启动一个静态 Web 服务器，将 ``dist`` 文件夹运行在 [http://localhost:5000](http://localhost:5000)。
这样在本地环境下查看该构建产物是否正常可用就方便了。

你可以通过 ``--port`` 参数来配置服务的运行端口。
````shell
{
  "scripts": {
    "preview": "vite preview --port 8080"
  }
}
````

## 发布
上面提到：默认情况下，构建会输出到 ``dist`` 文件夹中。你可以部署这个 ``dist`` 文件夹到任何你喜欢的平台。

### 集成到spring-boot
大多数公司后端都是采用 [spring-boot](https://spring.io/projects/spring-boot) 框架，集成也简单，只需要把 ``dist`` 目录下所有文件，
拷贝到``spring-boot``应用的 ``src/main/resources/staitc`` 静态目录下，后端打 ``jar`` 包时一起打包进去就可以了。

?> 找不到 ``staitc`` 目录的话，就在 ``src/main/resources`` 下新建一个。关于 ``spring-boot`` 的具体打包请查看官网文档。

### 集成到eggjs
作者喜爱的后端框架 [eggjs](https://eggjs.org/zh-cn/intro/) 框架，集成也简单，只需要把 ``dist`` 目录下所有文件，
拷贝到``egg``应用的 ``app/public`` 静态目录下，和后端一起部署就行了。

?> 找不到 ``public`` 目录的话，就在 ``app/public`` 下新建一个，关于eggjs部署请查看 [eggjs-应用部署](https://eggjs.org/zh-cn/core/deployment.html)。

### 前端独立部署
前端独立部署，一般是使用 [nginx](http://nginx.org/en/download.html) 做静态服务，只需要访问服务地址即可。

!> **注意**，如果后端没开跨域，需要nginx做 ``api接口转发代理``。

如果后端已经开跨域，nginx起个服务就行。

网上有很多教程：
[百度：nginx部署vue项目](https://www.baidu.com/s?wd=nginx部署vue项目)、[百度：nginx部署vue项目做代理](https://www.baidu.com/s?wd=nginx部署vue项目做代理)
