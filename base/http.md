## 前端请求流程
在 ``egg-admin-pro`` 中，一个完整的前端 UI 交互到服务端处理流程是这样的：

![前端请求流程](../_media/前端请求流程.png ':size=50%')

从上面的流程可以看出，为了方便管理维护，统一的请求处理都放在 ``@/api`` 文件夹中，并且一般按照 model 纬度进行拆分文件，如：

````shell
api/
  user.js
  xxx.js
  ...
````

## request.js
项目中``@/api/request.js`` 是基于 [axios](https://github.com/axios/axios) 的封装，便于统一处理 POST，GET 等请求参数，请求头，以及错误提示信息等。
具体可以查看 ``request.js``。 它封装了全局 ``request拦截器``、``response拦截器``、``统一的错误处理``、``统一做了超时处理``、``baseURL设置等``。

## 登录示例

````js
// @/api/user.js
import request from './request'
export function login(data) {
  return request({
    url: '/login',
    method: 'post',
    data
  })
}

// 使用时
import { login } from '@/api/user'
login({ userName: 'admin', passWord: 'password' }).then(res => {
    console.log(res)
}).catch(error => {
    console.log(error)
})
// 或者使用async await
async function getData() {
    const res = await login({ userName: 'admin', passWord: 'password' })
    console.log(res)
}
````

## 设置api请求地址
项目中使用 ``APP_BASE_API`` 环境变量来设置 api请求地址。

很多时候，我们需要根据不同环境请求不同的api接口地址，比如：开发环境一般是局域网地址``192.168.6.168``，生产环境可能是域名或者公网ip地址，
这就需要根据开发环境和生产环境来设置不同的 api请求地址。

我们可以通过[环境变量](base/env.md)区分环境并设置不同的 APP_BASE_API，详见[api请求 示例](base/env?id=api请求-示例)

?> 实际api请求地址是：变量``APP_BASE_API``的值 + 你的接口request.url 。

## 设置多个api请求地址
我们可以通过[环境变量](base/env.md)设置多个 api请求地址 变量，从而请求不同的 api 地址。
