页面整体布局是一个产品基本的框架结构，一般会包含导航、侧边栏、面包屑以及内容、版权等。当然这块也不复杂。

## layout

(点击图片看大图)

![项目布局图](../_media/layout1.x.jpg ':size=70%')

?> 对应代码 ``@/layouts/base-layout``


``egg-admin-pro`` 项目中大部分页面都是基于这个 ``layout`` 的，除了个别页面如：``login`` , 404, 401 等单页面没有使用该layout。
如果你想有多种不同的layout也是很方便的，项目预留有坑位，你可以在``@/layouts``目录下写你自定义的布局组件，只要在路由那里加载不同的layout组件就行。

dashboard主控台路由示例

````js
// 加载默认布局组件
import layout from '@/layouts/base-layout/index.vue'

const pre = 'dashboard-'

export default {
  path: '/dashboard',
  component: layout, // 这里可以写你自定义的layout组件
  meta: { name: 'Dashboard', iconSvg: 'menu-dashboard' },
  redirect: {
    name: `${pre}home`
  },
  children: [
    {
      path: 'home',
      name: `${pre}home`,
      meta: { name: '主控台', affix: true },
      component: () => import('@/views/dashboard/home.vue')
    },
    {
      path: 'monitor',
      name: `${pre}monitor`,
      meta: { name: '监控页', },
      component: () => import('@/views/dashboard/monitor.vue')
    }
  ]
}
````
## router-view

布局的滚动使用的是 [el-scrollbar](https://element-plus.gitee.io/#/zh-CN/component/scrollbar)，
[router-view](https://next.router.vuejs.org/zh/api/#router-view-%E7%9A%84-v-slot)里面，采用 [transition](https://v3.cn.vuejs.org/api/built-in-components.html#transition)做过渡动画，
[keep-alive](https://v3.cn.vuejs.org/api/built-in-components.html#keep-alive)是为了缓存组件预留坑位。
````html
<el-scrollbar ref="scrollbarRef" view-class="scrollBody flexC" wrap-class="flexC" class="flexC">
    <router-view v-if="isPageReload" v-slot="{ Component }">
      <transition name="fade-transform" mode="out-in">
        <keep-alive :include="cachedViews">
          <component :is="Component" />
        </keep-alive>
      </transition>
    </router-view>
    <!--版权-->
    <e-copyright v-if="layout.setting.layout.copyrightShow" />
</el-scrollbar>
````
!> **值得一提的是**，如果你的项目比较庞大，使用缓存[keep-alive](https://v3.cn.vuejs.org/api/built-in-components.html#keep-alive)，可以避免Vue组件没及时销毁而导致内存溢出问题。

## 多端适配

项目中使用[css3媒体查询](https://www.runoob.com/cssref/css3-pr-mediaquery.html)，
搭配[window.matchMedia](https://www.runoob.com/jsref/met-win-matchmedia.html)，实现``pc``、``平板``、``手机``、并支持``横竖屏适配``，你可以在此基础上适配更多设备。
