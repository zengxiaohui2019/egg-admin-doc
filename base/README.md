
[![vue-next](https://img.shields.io/badge/vue--next-3.1.5-brightgreen)](https://github.com/vuejs/vue-next)
[![element-plus](https://img.shields.io/badge/element--plus-1.0.2--beta.40-brightgreen)](https://github.com/element-plus/element-plus)
[![vite](https://img.shields.io/badge/vite-2.0.4-brightgreen)](https://github.com/vitejs/vite)
[![vite](https://img.shields.io/badge/release-1.0.0-blue)](https://github.com/zengxiaohui2019/egg-admin-template)

egg-admin-pro 是一个自主研发的``高颜值`` ``高性能`` ``多端适配``企业级通用后台前端解决方案，它基于 ``vite2.x`` 和 ``vue3.x`` 搭配 ``elementPlus``实现。它使用了最新的前端技术栈，
内置了 ``动态路由``、``权限验证``、``多端适配``、``自定义指令``, 提炼了典型的业务模型，提供了丰富的功能组件和工具，它可以帮助你快速构建企业级中后台产品原型。

## 预览地址
?> [egg-admin-pro 预览地址](https://github.zengxiaohui.com/egg-admin-pro/dist)、
[egg-admin-template 预览地址](https://github.zengxiaohui.com/egg-admin-template/dist)

> 建议
> 
> 本项目的定位是后台集成方案，不推荐直接当基础模板来进行二次开发。因为本项目集成了一些你可能用不到的功能，会造成不少的代码冗余。
> 
> 因此，特别提供了开源免费基础版本Template，详见 [版本对比](version/README?id=功能点)。
> 
> 推荐的方法是：当需要用到Pro版内置的丰富的业务模型和组件或者工具时，迁移到基础版中。
> - 专业版Pro：[egg-admin-pro 获取方式](version/README?id=商用说明)
> - 基础板Template： [github](https://github.com/zengxiaohui2019/egg-admin-template) <span mr10></span> [gitee](https://gitee.com/zengxiaohui2019/egg-admin-template)
>


## 功能

- ``兼容常见浏览器``

代码采用``vite``打包,``vue3``编写，默认支持现代浏览器。

!> 注意：IE等传统浏览器可以通过 [vite官方插件](https://github.com/vitejs/vite/tree/main/packages/plugin-legacy) 支持，注意：传统浏览器未经测试。

- ``覆盖常见业务页面``

从登录、到主控台、监控页、系统管理、表单列表详情、个人中心设置、富文本编辑器、等。

- ``http请求拦截封装``

采用``axios`` 进行统一封装，方便接口发起和返回拦截处理。

- ``按钮级鉴权``

根据登录用户的权限可生成``动态路由``和``动态菜单``，可精确到按钮级元素鉴权。

- ``自适应布局``

可根据视窗大小，自动调整布局，内置常见设备自适应，比如：``pc`` ``ipad`` ``iphone``。

- ``代码注释友好、利于二次开发``

在保证代码质量的前提下，注释力求详细，非常``利于代码阅读``，``便于二次开发``。


- ``多个常用组件``

内置多个常用的组件，比如：``svgIcon组件`` ``动态数字`` ``城市选择`` ``echart自适应组件`` ``趋势标记`` ``富文本编辑器`` 等。

- ``常用工具函数封装``

内置多个常用的工具函数或指令，比如：``时间处理`` ``判断数据为空`` ``监听dom变化指令`` ``鉴权指令``。

- 更多内容，期待你的体验。

## 前序准备

你需要在本地安装：[nodejs](https://nodejs.org/en/download/)、[git](https://git-scm.com/) ，``nodejs是必须`` git没有也无妨。
本项目技术栈基于 
[ES2015+](https://www.runoob.com/w3cnote/es6-tutorial.html)、
[vite2.x](https://cn.vitejs.dev)、
[vue3.x](https://v3.cn.vuejs.org)、
[vuex4.x](https://next.vuex.vuejs.org)、
[vue-router4.x](https://next.router.vuejs.org/zh/index.html) 、
[axios](https://www.kancloud.cn/yunye/axios/234845) 和 [element-plus](https://element-plus.gitee.io/#/zh-CN)，
登录和退出使用了在线mock模拟 [fastmock](https://www.fastmock.site/)，请确保联网，提前了解和学习这些知识会对使用本项目有很大的帮助。

## 总架构图

?> eggAdminPro 总架构图，建议使用前先熟悉整体架构图，以便能够快速使用。(点击图片看大图)

![项目总架构图](../_media/EggAdmin1.x.jpg ':size=70%')

?> eggAdminPro 核心架构图。[看高清大图](https://gitee.com/zengxiaohui2019/egg-admin-doc/raw/master/_media/EggAdmin1.x核心(src).jpg)

![核心架构图](../_media/EggAdmin1.x核心(src).jpg ':size=50%')

## 开发环境

为避免开发环境或依赖库不一致带来莫名其妙的问题，特说明以下。

- 操作系统：不限，作者用的是``mac``
- nodejs：>= 12，作者用的是``v14.16.1`` 查看版本方式：控制台输入``node -v``
- 浏览器：新版现代浏览器，最好是Chromium内核，比如：``Edge`` ``Chrome``  作者用的是``Edge``最新版

## 安装项目

!> 获取Pro版 详见[商用说明](version/README?id=商用说明)

- 下载项目 (未安装git的请从代码仓库上打包后下载)
````shell
    git https://gitee.com/zengxiaohui2019/egg-admin-template.git
````

- 进入项目根目录，安装依赖 NPM/YARN (作者用的是npm)
````shell
    cd 你的文件夹/egg-admin-template
    npm i
````

!> npm 默认用淘宝镜像，项目根目录``.npmrc``文件里面已经配置。

## 运行项目

````shell
    npm run dev
````
当控制台 出现  ```> Local: http://localhost:4000/``` 说明运行成功，点击地址或复制到浏览器地址栏，进行访问

默认会打开登录页，如下图：

![登录页](../_media/login.png ':size=50%')

?> 项目登录使用的是在线mock [fastmock](https://www.fastmock.site/)，请确保联网。

## 贡献

本文档项目地址 [egg-admin-doc](https://gitee.com/zengxiaohui2019/egg-admin-doc) 基于 [docsify](https://docsify.js.org/) 开发。

有任何修改和建议都可以该项目 pr 和 issue

项目还在持续迭代中，逐步沉淀和总结出更多功能和相应的实现代码，总结中后台产品模板/组件/业务场景的最佳实践。本项目也十分期待你的参与和[反馈](https://github.com/zengxiaohui2019/egg-admin-template/issues)

## 群聊

?> 加作者微信```badiweier```，拉你进群，群里作者会不定期回复，非必要尽量不要直接@作者（谢谢）。

![微信](../../_media/weixin.jpg ':size=300px')

## 鸣谢

- 感谢```vite团队```、```vue团队```、 ```element团队```、```感谢助我成长的开源项目```。
