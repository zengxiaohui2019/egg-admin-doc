如果熟悉 [vue-router](https://next.router.vuejs.org/zh/api/)  就很简单了。

## 一级目录(单页面)

比如：**新增一个列表页面**

### 1.新加路由文件

在[路由模块](base/router-nav.md?id=路由)中增加你需要添加的路由。 在``@/router/modules``目录新建 ``list.js``。 写入路由配置，代码如下：

````js
// 引入layout
import layout from '@/layouts/base-layout/index.vue'
// name前缀
const pre = 'list-test-'
export default {
    path: '/list-test',
    component: layout,
    meta: { name: '列表测试页面', icon: 'el-icon-tickets' },
    children: [
        {
            path: 'basic-list',
            name: `${pre}basic-list`,
            meta: { name: '基础列表'  },
            component: () => import('@/views/list-test/basic-list.vue')
        }
    ]
}
````
?> **小贴士**，项目侧栏默认使用嵌套路由来递归渲染菜单，你需要配置子路由``children``。

!> **注意**，路由的 ``name`` 属性必须保持全局唯一，并且和对应vue文件的name属性保持一致，组件缓存``keep-alive``使用的是name作为唯一值。

### 2.导入到 ``routes.js``

放在主框架内不需要鉴权的路由数组中。代码如下：

````js
// 导入listTest路由模块
import listTest from './modules/list-test'
/**
 * 主框架内 不需要鉴权的路由
 */
export const layoutNoPermission = [
    {
        path: '/',
        redirect: {
            name: 'dashboard-home'
        },
        meta: { hidden: true }
    },
    // ...这里省略一些代码,
    listTest // 加载在这里
]
````

### 3.新增view页面
根据目录``@/views/list-test/basic-list.vue``，新加Vue文件，代码如下：
````html
<template>
  <div>list-test</div>
</template>

<script>
export default {
  name: "list-test"
}
</script>

<style scoped lang="scss">

</style>
````
目录如图所示：

![新增view页面](../_media/list-test-view.png ':size=240px')


?> ``<template>``里面为何要嵌套一个根``<div>``，虽然vue3.x支持多个根元素，但是项目中根据页面根``<div>``设置布局容器的高度，你可以不加入根``<div>``试试效果，(不过不建议这么做)。

这个时候，侧栏会出现刚刚新添加的菜单，如下图:

![新添加的菜单](../_media/list-test.png)

你会发现路由是嵌套的，如果不想这样，可以配置成单页面即可，根路由``meta``属性里面配置``singlePage: true``即可避免这一特性。代码如下：

````js
// 引入layout
import layout from '@/layouts/base-layout/index.vue'
// name前缀
const pre = 'list-test-'
export default {
  path: '/list-test',
  component: layout,
  meta: { name: '列表测试页面', icon: 'el-icon-tickets', singlePage: true },
  children: [
    {
      path: 'basic-list',
      name: `${pre}basic-list`,
      meta: { name: '基础列表'  },
      component: () => import('@/views/list-test/basic-list.vue')
    }
  ]
}
````
效果如下图:

![新添加的菜单1](../_media/list-test1.png)

?> **好像有点问题，没有图标**，因为根路由``meta``属性里面配置``singlePage: true``，会只渲染子路由，所以你需要在子路由``meta``属性里面配置icon，代码如下：

````js
// 引入layout
import layout from '@/layouts/base-layout/index.vue'
// name前缀
const pre = 'list-test-'
export default {
  path: '/list-test',
  component: layout,
  meta: { name: '列表测试页面', icon: 'el-icon-tickets', singlePage: true },
  children: [
    {
      path: 'basic-list',
      name: `${pre}basic-list`,
      meta: { name: '基础列表', icon: 'el-icon-tickets' },
      component: () => import('@/views/list-test/basic-list.vue')
    }
  ]
}
````
效果如下图:

![新添加的菜单3](../_media/list-test3.png)

侧栏收起来效果:

![新添加的菜单4](../_media/list-test4.png)


## 多级目录(嵌套页面)

实现嵌套路由很简单，只需要在根路由``children``新建多个路由，子路由后面还可以有子路由，项目会自动递归路由生成嵌套侧栏菜单。

把上面的路由多几层嵌套 ``singlePage: true``记得去掉，否则渲染的是多个单页面，代码如下：

````js
// 引入layout
import layout from '@/layouts/base-layout/index.vue'
// name前缀
const pre = 'list-test-'
export default {
  path: '/list-test',
  component: layout,
  meta: { name: '列表测试页面', icon: 'el-icon-tickets' },
  children: [
    {
      path: 'basic-list',
      name: `${pre}basic-list`,
      meta: { name: '基础列表', icon: 'el-icon-tickets' },
      component: () => import('@/views/list-test/basic-list.vue')
    },
      {
          path: 'basic-list-1',
          name: `${pre}basic-list-1`,
          meta: { name: '基础列表-1', icon: 'el-icon-tickets' },
          component: () => import('@/views/list-test/basic-list.vue'),
          children: [
              {
                  path: 'basic-list-1-1',
                  name: `${pre}basic-list-1-1`,
                  meta: { name: '基础列表-1-1', icon: 'el-icon-tickets' },
                  component: () => import('@/views/list-test/basic-list.vue')
              },
              {
                  path: 'basic-list-1-2',
                  name: `${pre}basic-list-1-2`,
                  meta: { name: '基础列表-1-2', icon: 'el-icon-tickets' },
                  component: () => import('@/views/list-test/basic-list.vue')
              }
          ]
      },
      {
          path: 'basic-list-2',
          name: `${pre}basic-list-2`,
          meta: { name: '基础列表-2', icon: 'el-icon-tickets' },
          component: () => import('@/views/list-test/basic-list.vue')
      }
  ]
}
````

效果如图：

![嵌套路由](../_media/list-test嵌套路由.png)

?> 如果子路由不希望有图标，在``meta``属性去掉icon配置即可。

!> **仅做演示效果**，实际项目应该确保每个路由和vue文件name全局唯一。


## 新增 api

最后在 ``@/api`` 文件夹下创建本模块对应的 api 服务。

## 新增组件

推荐写 vue 项目的习惯，在全局的 ``@/components`` 只会写一些全局的组件，如富文本，各种图表组件，列表组件，封装的常用组件等，能被公用的组件。
每个页面或者模块特定的业务组件则会写在当前 views 下面。如：@/views/list/components/xxx.vue。这样拆分大大减轻了维护成本，找起来特别方便。

**个人认为：拆分组件最大的好处是在公用的同时保证可维护性！**，便于他人协作，我见过有些项目，找个代码找半天。

## 新增样式

页面的样式和组件是一个道理，全局的 ``@/styles`` 放置一下全局公用的样式，
每一个页面的样式就写在所属vue文件里面，请记住加上``scoped`` 或者命名空间、``css module``也可以(直接变量形式使用更方便)，避免造成全局的样式污染。
