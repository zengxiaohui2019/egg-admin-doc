## css 预处理器
项目中已内置 ``sass``预处理器，使用时也很简单，详细文档请查看[Vite CSS 预处理器](https://cn.vitejs.dev/guide/features.html#css-pre-processors)

如果是用的是单文件组件，可以通过 ``<style lang="scss">``自动开启。
````html
<template>
    <div class="test-div">test</div>
</template>
<style scoped lang="scss">
    .test-div{
        background-color: pink;
    }
</style>
````
## css modules

在样式开发过程中，有两个问题比较突出：

- 全局污染 —— 因为css选择器是全局生效，根据打包顺序后面的css会覆盖前面的css。
- 选择器复杂  —— 为避免上面的问题，我们总是为了一个class类选择器名称苦恼，变得越来越长，导致风格也不一致，不易维护。

### scoped
vue为我们提供了 ``<style scoped>`` 可以很方便解决上述问题，它的原理就是给css加上独立作用域。
````css
/* 编译前 */
.example {
  color: red;
}

/* 编译后 */
.example[_v-f3f3eg9] {
  color: red;
}
````
只要加上 ``<style scoped>`` 这样 css 就只会作用在当前组件内了

### modules
任何以 ``.module.css`` 为后缀名的 CSS 文件都被认为是一个 [CSS modules](https://github.com/css-modules/css-modules) 文件。导入这样的文件会返回一个相应的模块对象：
````css
/* example.module.css */
.red {
  color: red;
}
````
````js
// 你可以在js中直接使用
import classes from '你的路径/example.module.css'
document.getElementById('foo').className = classes.red
````
CSS modules 行为可以通过 [css.modules](https://cn.vitejs.dev/config/#css-modules) 选项 进行配置，配置文件是项目根目录下的 ``vite.config.js``。

如果 ``css.modules.localsConvention`` 设置开启了 ``camelCase`` 格式变量名转换（例如 ``localsConvention: 'camelCaseOnly'``），你还可以使用按名导入。
````js
// .apply-color -> applyColor
import { applyColor } from './example.module.css'
document.getElementById('foo').className = applyColor
````
?> 关于 [CSS modules](https://cn.vitejs.dev/guide/features.html#css-modules)，请查看vite官网查看，这里只是简单介绍一下。

## 目录结构

项目中使用的全局样式都在 ``@/styles``目录下，其中布局的在 ``@/styles/layout``设置。
````shell
├─styles
├── layout 布局
│   └── base-layout
│     ├── layout.module.scss 布局module
│     ├── layout.scss 布局样式
│     └── menu.module.scss 侧栏菜单module
├── color.scss 颜色
├── element-ui.scss 覆盖element-ui样式
├── index.scss 默认样式
└── transition.scss 过渡动画
````
## 导入css或scss

### ``<style>``中导入
使用``@import '你的地址.scss';``，**``<style></style>``标签里面``;``分号不能少**。
````html
<style lang="scss" scoped>
    @import '你的路径/layout.scss';
    @import '你的路径/layout.css';
</style>
````

### js中导入
使用``import '你的地址.scss'``

````js
import '@/styles/index.scss'
import '@/styles/index.css'
````

## 覆盖element-ui样式
在很多时候，UI库提供的样式并不能满足我们需求，那么就需要修改它的样式，下面介绍两种方式：

1. 父组件加一个 ``class``：在父组件上直接绑定一个class类选择器，或者利用UI组件给你暴露的配置class名称属性，比如：``el-scrollbar``组件提供的``wrap-class``属性。
2. 深度选择器(推荐)：``::v-deep``看下文。

?> 父组件加一个 class 的方式有的时候不一定有效，推荐使用深度选择器 ``::v-deep``

## 父改子样式 深度选择器
很多时候，我们需要父组件修改子组件样式，最常见的就是覆盖UI库给我们提供的默认样式，我们使用 ``::v-deep`` 就可以实现样式穿透。

### 修改``el-tag``示例
````html
<template>
    <div class="test-div">
        <el-tag type="success">标签二</el-tag>
    </div>
</template>
<style scoped lang="scss">
    // 深度方式
    .test-div{
        ::v-deep .el-tag{
            background-color: pink;
        }
    }
</style>
````

!> vue2.x的 ``/deep/`` vue3.x已经不再支持，请使用``::v-deep``

## postcss
[PostCSS](https://github.com/postcss/postcss) 是一个允许使用 JS 插件转换样式的工具。 这些插件可以检查（lint）你的 CSS，支持 CSS Variables 和 Mixins，编译尚未被浏览器广泛支持的先进的 CSS 语法，内联图片，以及其它很多优秀的功能。
由于postcss很强大便捷，vite官方也提供了集成方式详见[vite PostCSS 配置](https://cn.vitejs.dev/guide/features.html#postcss)
