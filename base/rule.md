在实际业务中，会经常遇到需要根据角色或者用户动态生成侧栏菜单，对于某些按钮的操作进行管控，本项目里已经内置两大功能：``动态路由``、``指令鉴权``

## 动态路由
动态路由也就是动态生成侧栏菜单，实现方式如下：

- **简单的角色用户路由设置**：后端返回权限id集合，前端根据约定绑定到路由，从而实现动态路由，特点：``简单``、``好理解``。
- **复杂高逼格一点的**：路由表保存在后端，根据后端返回路由表异步生成动态路由，特点：``高度可配置化``、``方便维护``。
- **网上还有其他野路子**：有兴趣可以自行摸索。

这里，项目中选择 ``简单的角色用户路由设置`` 的方式，能满足大部分业务场景需求，

在[路由配置](base/router-nav?id=配置项)有提到 ``meta``属性里有个``ruleId``属性，
它是前后端约定好的每个路由的唯一权限id，如果根路由设置权限，子路由会自动继承该权限，当然你也可以在子路由单独设置。路由权限过滤核心代码如下：

?> 代码地址：``@/store/modules/permission.js``

### 示例

假定：有一个``系统管理模块(权限id=12)``，拥有``用户管理(权限id=13)``子模块，用户管理拥有：``新增(权限id=14)``、``编辑(权限id=15)``、``删除(权限id=16)``等按钮操作。
动态生成侧栏菜单只需要在对应路由``meta``属性配上``ruleId``即可。[按钮鉴权](#指令鉴权)

路由结构如下配置
````js
import layout from '@/layouts/base-layout/index.vue'
const pre = 'system-'

export default {
  path: '/system',
  component: layout,
  meta: { name: '系统管理', iconSvg: 'menu-system', ruleId: 12 },
  redirect: {
    name: `${pre}user`,
  },
  children: [
    {
      path: 'user',
      name: `${pre}user`,
      meta: { name: '用户管理', ruleId: 13 },
      component: () => import('@/views/system/user.vue')
    }
  ]
}
````

## 指令鉴权
项目封装了一个指令权限，能简单快速的实现按钮级别的权限判断。 ``v-auth``，详见``权限校验插件``

?> 代码地址：``@/plugins/EAuth/index.js``

v-auth默认值是一个数组，支持权限叠加，比如：
````html
单个权限
<div v-auth="[13]"></div>

权限叠加
<div v-auth="[12,13]"></div>
````

!> **注意**：权限id数据来源于vuex``store.state.user.rules``，所以登录时需要返回``rules``数组字段，当然你也可以自己修改数据来源，**注意异步问题**。

### 示例

继续使用上面的示例，我们只需要在``@/views/system/user.vue``文件里面写上``v-auth=[对应权限id]``即可。
````html
<template>
    <div class="flexR option">
        <el-button v-auth="[14]" type="primary" round plain icon="el-icon-plus" @click="AddEdit({}, 0)">
            新增
        </el-button>
        <el-button v-auth="[15]" type="primary" round plain icon="el-icon-edit">
            编辑
        </el-button>
        <el-button v-auth="[16]" type="danger" round plain icon="el-icon-delete">
            删除
        </el-button>
    </div>
</template>
````
