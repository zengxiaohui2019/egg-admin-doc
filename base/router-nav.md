路由和侧边栏(也叫菜单)是组织起一个后台应用的关键部分。

本项目侧边栏是和路由一起绑定的，意味着你必须在 ``@/router/index.js`` 中配置对应路由，就可以实现侧边栏自动动态加载，当然这需要遵循一些配置规则。

## 配置项
首先我们了解一下本项目配置路由时提供了哪些配置项，规则在路由``meta``属性里面配置，代码中也有详细注释。

|   配置项|   类型    |   必填    |    名称          |    备注
|   -----    |   :---:   | :---:    |    ----         |    ----
|   name    |   String   |     是   |    菜单名称       |    必须和组件name保持一致
|   icon    |   String   |     否   |    饿了么UI的图标  |
|   iconSvg |   String   |     否   |    svg图标       |    加载的是 src/icons 目录下的svg图标名称(不包含.svg后缀)，svg图标如何使用，请参考``@/components/SvgIcon/index.vue``
|   hidden  |   Boolean  |     否   |    菜单是否隐藏  |    默认false
|   showTab |   Boolean  |     否   |    菜单是否显示在多页签中  |    默认true
|   cache   |   Boolean  |     否   |    页面是否缓存  |    默认false
|   affix   |   Boolean  |     否   |    标签是否始终粘贴在多页签中  |    默认false
|   singlePage| Boolean  |     否   |    是否是单页面  |    如果只有一个子路由默认会渲染嵌套菜单，设置此属性可只渲染子菜单，默认false
|   ruleId  | Number     |     否   |    权限id       |    前后端约定好的每个路由的唯一权限id，如果根路由设置权限，子路由会自动继承该权限，当然你也可以在子路由单独设置

!> ``icon`` ``iconSvg``必须二选一，二者可并存，但iconSvg优先级更高，如果两个都没设置，在侧栏收起时，无法展示菜单图标。

``component: () => import('@/views/xxx.vue')`` 这种加载方式是[路由懒加载](https://next.router.vuejs.org/zh/guide/advanced/lazy-loading.html)，异步懒加载的意思：默认不加载，只有需要了才会加载。

示例：

````js
const pre = 'system-'

export default {
    path: '/system',
    component: layout,
    meta: { 
        name: '系统管理', // 菜单名称
        iconSvg: 'menu-system', // svg图标
        ruleId: 12, // 权限id 如果根路由设置权限，子路由会自动继承该权限，当然你也可以在子路由单独设置.
        hidden: false, // 设置菜单显示
        showTab: true, // 显示在多页签中
        singlePage: false, // 渲染嵌套菜单
    },
    redirect: {name: `${pre}user`}, // 重定向地址
    children: [
        {
            path: 'user',
            name: `${pre}user`,
            meta: { name: '用户管理', ruleId: 13, cache: true },
            component: () => import('@/views/system/user.vue')
        },
        {
            path: 'menu',
            name: `${pre}menu`,
            meta: { name: '菜单权限管理',  },
            component: () => import('@/views/system/menu.vue')
        },
        {
            path: 'role',
            name: `${pre}role`,
            meta: { name: '角色权限管理',  },
            component: () => import('@/views/system/role.vue')
        }
    ]
}
````

## 路由

项目中的路由是分模块编写，然后统一加载到``routes.js``，这样便于维护。

?> 对应代码 ``@/router/modules`` -> ``@/router/routes.js``

这里路由分4种：``layoutNoPermission``、``layoutPermission``、``layoutOut``、``errorPage``。

- layoutNoPermission：主框架内(在layout布局中加载)，不需要鉴权的路由。
- layoutPermission：主框架内，需要动态鉴权并通过 [addRoutes](https://next.router.vuejs.org/zh/api/#addroute) 动态添加的页面。
- errorPage：错误提示页面, 例如：403，404，500页面，对于所有路由进行``/:path404(.*)*``[捕获处理](https://next.router.vuejs.org/zh/guide/essentials/dynamic-matching.html#%E6%8D%95%E8%8E%B7%E6%89%80%E6%9C%89%E8%B7%AF%E7%94%B1%E6%88%96-404-not-found-%E8%B7%AF%E7%94%B1)。
- layoutOut：主框架外，免登录，一般是登录、注册页面。

其它的配置和 [vue-router](https://next.router.vuejs.org/zh/api/) 官方并没有区别，自行查看文档。

!> errorPage必须放在layoutPermission路由之后加载，否则浏览器刷新页面直接进404页面。

## 侧边栏(菜单)

本项目侧边栏主要基于 [element-plus](https://element-plus.gitee.io/#/zh-CN) 的 [NavMenu 导航菜单](https://element-plus.gitee.io/#/zh-CN/component/menu) 改造。

前面提到，侧边栏是通过读取路由并结合权限判断而动态生成的，而且支持路由无限嵌套，所以这里还使用到了递归组件，[如何生成动态路由](/base/rule?id=%e5%8a%a8%e6%80%81%e8%b7%af%e7%94%b1)。

?> 对应代码 ``@/layouts/base-layout/menu-side``

当然，这里改造了``element-plus``的不少导航菜单默认样式，所有的 css 都可以在 ``@/styles/layout/menu.module.scss`` 中找到，你也可以根据自己的需求进行修改。

**这里需要注意一下**，一般侧边栏有两种形式即：submenu 和 直接 el-menu-item。 一个是嵌套子菜单，另一个则是直接一个链接。如下图：

![侧边栏](../_media/submenu.png ':size=200')

在路由递归中已经帮你做了判断，当你一个路由下面的 ``children`` 声明的路由大于>1 个时，自动会变成嵌套的模式。 否则就会默认将子路由作为根路由显示在侧边栏中，
若不想这样，可以通过设置在根路由中``meta``属性设置``singlePage: false``来取消这一特性。

## 多级菜单(嵌套路由)

如果你的路由是多级目录，如本项目 ``@/views/tool`` 那样(pro版才有)，有多级路由嵌套的情况下。如下图：

![侧边栏](../_media/多级菜单.png ':size=200')

你可以这样编写路由，**需要注意的是**，嵌套子路由``component``属性必须配一个多级路由模板``@/layouts/base-layout/child-layout/index.vue``，
其实里面就一个``<router-view />`` ，理论上有多少层就写多少个。

````js
import layout from '@/layouts/base-layout/index.vue'
import layoutChild from '@/layouts/base-layout/child-layout/index.vue'

const pre = 'tool-'

export default {
  path: '/tool',
  name: 'tool',
  meta: { name: '工具', iconSvg: 'menu-tool' },
  redirect: {
    name: `${pre}iframe-baidu`
  },
  component: layout, // 主路由模板
  children: [
    {
      path: 'iframe',
      name: `${pre}iframe`,
      meta: { name: '内嵌页面' },
      component: layoutChild, // 多级路由模板 里面就一个<router-view /> 有多少层就写多少个
      redirect: {
        name: `${pre}iframe-baidu`
      },
      children: [
        {
          path: 'baidu',
          name: `${pre}iframe-baidu`,
          meta: { name: '百度', },
          component: () => import('@/views/tool/iframe/baidu.vue')
        },
        {
          path: 'eggAdmin',
          name: `${pre}iframe-egg-admin`,
          meta: { name: 'eggAdmin', },
          component: () => import('@/views/tool/iframe/eggAdmin.vue')
        }
      ]
    }
  ]
}
````

## 侧边栏 外链

这个后续会加上，当前版本暂时没有.
