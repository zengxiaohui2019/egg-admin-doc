实际业务中经常用到本地静态资源，比如：``图片``、``svg``、``json``、``js``、``css``、``scss``等，
项目中依赖 [vite 静态资源处理](https://cn.vitejs.dev/guide/features.html#static-assets) 模块，详细文档还请查看官方文档，这里只做简单介绍。

## public静态目录
vite 将``public``目录作为静态目录，vite不会去理会目录下的任何文件。

如果你有下列这些资源：
- 不会被源码引用（例如 ``robots.txt``）
- 必须保持原有文件名（没有经过 hash）
- ...或者你压根不想引入该资源，只是想得到其 URL。

那么你可以将该资源放在指定的 ``public`` 目录中，它应位于你的项目根目录。该目录中的资源在开发时能直接通过 ``/`` 根路径访问到，并且打包时会被原封不动复制到目标目录(默认``dist``)的根目录下。

?> **请注意**： 引入 ``public`` 中的资源永远应该使用根绝对路径，举个例子：``public/icon.png`` 应该在源码中被引用为 ``/icon.png``。

## 图片
一般来说，图片会被解析成base64格式

````js
import imgUrl from './img.png' // jpg gif 等其他图片格式同理
console.log(imgUrl)
````

## svg
项目中内置 [svg解析工具 vite-plugin-svg-icons](https://github.com/anncwb/vite-plugin-svg-icons/blob/main/README.zh_CN.md)，只需要把svg放到 ``@/icons`` 目录下就可以解析，
推荐分模块新建一个目录来存放svg，比如：``@/icons/test``，便于查找维护。

假定：有这样一个``fly.svg``文件 ``@/icons/test/fly.svg``

组件中使用方式如下：
使用 ``svg-icon`` 组件加载，``<svg-icon :name="svg的二级路径-svg的文件名称" />``中间用``-``连接，如果是``@/icons``根目录不需要加二级路径。

最终``@/icons/test/fly.svg`` 加载为：
````html
<template>
    <div>
        <svg-icon :name="test-fly" />
    </div>
</template>
````
!> 我猜你可能觉得，有二级路径比较麻烦，其实在[vite-plugin-svg-icons 文档](https://github.com/anncwb/vite-plugin-svg-icons/blob/main/README.zh_CN.md)
中有配置``symbolId`` 不使用二级路径，如果你懂得修改后带来的后果 (**所有二级路径下的图片都加载不出来**) 并知道如何解决问题，你可以修改规则，**但我不建议这么做**。

## json
[vite内置(https://cn.vitejs.dev/guide/features.html#json)]对json文件的导入支持，摆脱了传统的http请求加载json的繁琐方式。

JSON 可以被直接导入 —— 同样支持具名导入：

````js
// 导入整个对象
import json from './example.json'
// 对一个根字段使用具名导入 —— 有效帮助 treeshaking！
import { field } from './example.json'
````

## 动态导入多个js模块
vite 支持使用特殊的 ``import.meta.glob`` 函数从文件系统导入多个模块：详见[Glob 导入](https://cn.vitejs.dev/guide/features.html#glob-import)

````js
const modules = import.meta.glob('./dir/*.js')
````
以上将会被转译为下面的样子：
````js
// vite 生成的代码
const modules = {
  './dir/foo.js': () => import('./dir/foo.js'),
  './dir/bar.js': () => import('./dir/bar.js')
}
````
你可以遍历 ``modules`` 对象的 key 值来访问相应的模块：
````js
for (const path in modules) {
  modules[path]().then((mod) => {
    console.log(path, mod)
  })
}
````
匹配到的文件默认是懒加载的，通过动态导入实现，并会在构建时分离为独立的 ``chunk``。如果你倾向于直接引入所有的模块（例如依赖于这些模块中的副作用首先被应用），请查看下文 直接导入多个js模块。

?> ``chunk``：通俗来讲，就是按照指定规则把代码拆成多个文件，理论上每一个动态导入的vue组件都会打包成一个独立的``chunk``。

## 直接导入多个js模块
从文件系统直接导入多个模块使用 ``import.meta.globEager``函数，示例：
````js
const modules = import.meta.globEager('./dir/*.js')
````
以上会被转译为下面的样子：
````js
// vite 生成的代码
import * as __glob__0_0 from './dir/foo.js'
import * as __glob__0_1 from './dir/bar.js'
const modules = {
  './dir/foo.js': __glob__0_0,
  './dir/bar.js': __glob__0_1
}
````
!> **请注意**： 这只是一个 Vite 独有的功能而不是一个 Web 或 ES 标准,
</br>该 Glob 模式会被当成导入标识符：必须是相对路径（以 ``./`` 开头） 或绝对路径（以 ``/`` 开头，相对于项目根目录解析）。

## 导入css或scss
请查看 [style 导入css或scss说明](base/style?id=导入css或scss)

## new URL
[import.meta.url](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/import.meta) 是一个 ESM 的原生功能，
会暴露当前模块的 URL。将它与原生的 [URL 构造器](https://developer.mozilla.org/en-US/docs/Web/API/URL) 组合使用，
在一个 JavaScript 模块中，通过相对路径我们就能得到一个被完整解析的静态资源 URL：

````js
const imgUrl = new URL('./img.png', import.meta.url)
document.getElementById('hero-img').src = imgUrl
````

这在现代浏览器中能够原生使用 - 实际上，Vite 并不需要在开发阶段处理这些代码！

这个模式同样还可以通过字符串模板支持动态 URL：
````js
function getImageUrl(name) {
  return new URL(`./dir/${name}.png`, import.meta.url).href
}
````
在生产构建时，Vite 才会进行必要的转换保证 URL 在打包和资源哈希后仍指向正确的地址。
