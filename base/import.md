除了 ``element-plus`` 组件以及项目内置的常用组件，有时我们还需要引入其他npm外部组件，有的是vue插件、或者原生js插件，下面是两种插件示例：


## vue插件

这里以引入 [CKEditor5 富文本vue3.x](https://ckeditor.com/docs/ckeditor5/latest/builds/guides/integration/frameworks/vuejs-v3.html) 为例进行介绍。

!> **注意**：``vue2.x``版本的插件不兼容``vue3.x``，有兴趣的话可以自己改造，参考[vue官网 编写插件](https://v3.cn.vuejs.org/guide/plugins.html#编写插件)。

### 安装
按照官网教程，复制代码进控制台，执行安装。

````shell
npm install --save @ckeditor/ckeditor5-vue @ckeditor/ckeditor5-build-classic
````
?> 细心的你会发现，为何安装两个东东？
</br>因为 CKEditor5 有[多个版本](https://ckeditor.com/docs/ckeditor5/latest/examples/index.html)、
[classic 经典版](https://ckeditor.com/docs/ckeditor5/latest/examples/builds/classic-editor.html)、
[Inline 内嵌版](https://ckeditor.com/docs/ckeditor5/latest/examples/builds/inline-editor.html)、
[Balloon block 气泡版](https://ckeditor.com/docs/ckeditor5/latest/examples/builds/balloon-block-editor.html)等，官方拆成多个js库分开维护。
</br>
``@ckeditor/ckeditor5-vue`` ：CKEditor5适配vue的容器(作者本人是这么理解的)，
</br>
``@ckeditor/ckeditor5-build-classic`` ： ``classic 经典版`` 的核心js库，
</br>
**一般来说，大多数vue插件，安装一个就行**。

npm包下载好后会自动添加依赖到 ``package.json`` 中去，方便管理。

?>``--save`` 可简写``-S`` 代表在生产和开发环境都要使用，依赖会保存在 ``package.json``的``dependencies``中。
</br>``--dev`` &nbsp;&nbsp;可简写``-D``  代表只在生产环境使用(一般是一些构建工具)，依赖会保存在 ``package.json``的``devDependencies``中。

### 全局注册
全局注册：可以在页面中任何位置使用，一般推荐 **使用频率高的组件** 使用全局注册，大量全局组件会加大浏览器性能消耗。

vue插件会暴露一个 ``install`` 函数，我们可以直接在项目入口js ``main.js`` 中使用 ``use`` 直接安装，代码示例：

````js
// main.js
import { createApp } from 'vue'
import App from './App.vue'
// 导入 这里只安装CKEditor5适配vue的容器
import CKEditor from '@ckeditor/ckeditor5-vue'
const app = createApp(App)
// 安装
app.use(CKEditor)
// 这里省略了一些无关代码
app.mount('#app')
````

.vue文件中使用
````html
<template>
    <div>
        <ckeditor v-model="editorData" :editor="editor" :config="editorConfig" />
    </div>
</template>
<script>
    // 经典版核心js库
    import ClassicEditor from '@ckeditor/ckeditor5-build-classic'
    // 导入中文语言包
    import '@ckeditor/ckeditor5-build-classic/build/translations/zh-cn'
    export default {
        name: 'editor-ck-editor',
        setup() {
            return {
                editor: ClassicEditor, // 经典版核心js库
                editorData: '<p>Content of the editor.</p>',
                editorConfig: {
                    language: 'zh-cn',
                }
            }
        }
</script>
<style scoped lang="scss">
    .editor-ck-editor-page{
        padding-top: 20px;
        ::v-deep .ck-editor__editable_inline {
            // 设个最小高度
            min-height: 500px !important;
        }
    }
</style>
````
### 局部注册
局部注册：组件用到了再加载，可以减少浏览器性能消耗，只能在注册了的组件中使用，一般推荐 **使用频率低的组件** 使用局部注册。

?> 就多了两个步骤 ``导入适配vue的容器`` ``导出component组件 供组件template使用``，其他都一样。

````html
...
<script>
    // 导入适配vue的容器
    import CKEditor from '@ckeditor/ckeditor5-vue'
    // 经典版核心js库
    import ClassicEditor from '@ckeditor/ckeditor5-build-classic'
    // 导入中文语言包
    import '@ckeditor/ckeditor5-build-classic/build/translations/zh-cn'
    export default {
        name: 'editor-ck-editor',
        components: {
            // 导出component组件 供组件template使用
            ckeditor: CKEditor.component
        },
        setup() {
            return {
                editor: ClassicEditor, // 经典版核心js库
                editorData: '<p>Content of the editor.</p>',
                editorConfig: {
                    language: 'zh-cn',
                }
            }
        }
</script>
...
````

## 引入原生js插件
**原生js插件**：就是不依赖任何js框架，它由纯js编写，兼容性很好。

这里以引入 [echarts](https://echarts.apache.org/zh/) 为例进行介绍。

### 安装
````shell
npm install echarts --save
````

### 全局使用
全局使用：一般是挂载在 [vue实例](https://v3.cn.vuejs.org/api/application-config.html#globalproperties) ``app.config.globalProperties``上，
或者挂载在 ``window`` 全局对象上，**建议变量以 ``$``作为前缀以示区分**，代码如下：

````js
// main.js
// 导入插件
import * as echarts from 'echarts'
// 挂载在vue实例上
app.config.globalProperties.$echarts = echarts
// 挂载在 window 全局对象上
window.$echarts = echarts

````
.vue组件中使用

````html
...
<script>
    // 从vue实例上获取
    import { getCurrentInstance } from 'vue'
    export default {
        name: 'echarts-page',
        setup() {
            const self = getCurrentInstance().appContext.config.globalProperties
            // console.log(self)
            console.log(self.$echarts)
            // 从window对象中获取
            console.log('从window对象中获取', window.$echarts)
        }
</script>
...
````
?> **事实上**，``element-plus``很多实例也是挂载在``app.config.globalProperties``上，你可以打印 ``console.log(self)`` 看看。

?> **需要说明的是**，vue官方更推荐使用 [provide和inject](https://v3.cn.vuejs.org/guide/component-provide-inject.html#provide-inject) 进行组件之间传递数据，具体请看官方文档。

### 局部使用
.vue组件中引入，直接使用即可。

````html
...
<script>
    import * as echarts from 'echarts'
    // 直接对着文档写你的业务逻辑即可
    export default {
        name: 'echarts-page',
        setup() {
            console.log(echarts)
        }
</script>
...
````
