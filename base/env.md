``egg-admin-pro`` 是基于 [vite](https://cn.vitejs.dev/guide)来进行构建，
所以所有的环境变量配置都是基于[vite环境变量](https://cn.vitejs.dev/guide/env-and-mode.html)来实现和控制的。

vite 从你的根目录中读取 ``.env文件`` 加载额外的环境变量，并通过 ``import.meta.env.你的变量`` 暴露给客户端使用。

## .env文件
``.env``是全局的，不管任何环境都会加载到环境变量中，可以放一些不需要根据环境变化的数据。

!> **特别注意**，只有以 ``VITE_`` 为前缀的变量才会暴露出来。

## .env.development文件
``.development``是开发环境才会加载到环境变量中，可以放一些开发环境使用的数据。

## .env.production文件
``.production``是生产环境才会加载到环境变量中，可以放一些生产环境使用的数据。

?> 生产环境是指：执行``build``命令

## api请求-示例
````env
# .env.development
VITE_APP_BASE_API = 'http://192.168.6.168/api'

````

````env
# .env.production
VITE_APP_BASE_API = 'https://www.eggadmin.com/api'

````
````js
// @/api/request.js
...
const service = axios.create({
  baseURL: import.meta.env.VITE_APP_BASE_API,
  timeout: 30000, // 请求超时时间 默认30秒
  headers: {
    'Content-Type': 'application/json'
  }
})
...
````
> 最终APP_BASE_API = 
开发环境：``http://192.168.6.168/api``
生产环境：``https://www.eggadmin.com/api``

?> 实际api请求地址是：``APP_BASE_API`` + 你的接口request.url 。

!> 如果有涉及跨域请查看[跨域代理](advanced/cross.md)。
