?> 系统配置文件 ``@/setting/index.js``

``@`` 又是什么东西？请查看[vite设置别名方式](https://cn.vitejs.dev/config/#resolve-alias)

## 配置项
| 属性      |  类型   |  默认值               |  必填    | 描述
| ---------| ------- | -----                | ------- | ------- 
| sysTitle |  String |  Egg-Admin-Pro       | 是      |  系统名称全称和页面标题的后缀
| logoTitle |  String |  Egg Admin Pro      | 是      |  系统名称简写 左上角logo地方展示
| indexPage |  String |  /                  | 是      |  默认首页路由地址
| loginPage |  String |  login              | 是      |  默认登录页路由地址
| routerMode |  String |  hash              | 是      |  路由模式，可选值为 ``history`` 或 ``hash``
| tokenKey   |  String |  egg_admin_token   | 是      |  默认cookie的key参数，这个是为了将来需要使用cookie而设计，当前版本暂未用到
| layout   |  Object |  详见[layout配置项](#layout配置项)   | 是      |  布局配置

## layout配置项
layout配置项列表，是设置[layout](#layout)布局效果。

| 属性               |  类型   |  默认值               |  必填    | 描述
| ---------         | ------- | -----                | ------- | ------- 
| themeType         |  String |  light       | 是      |  主题风格配置 ``dark`` (绅士黑) 或者 ``light`` (极简白)
| logoShow          |  Boolean|  true        | 是      |  是否显示logo
| uniqueOpened      |  Boolean|  true        | 是      |  左侧侧栏(也叫菜单)是否只保持一个子菜单的展开 默认true
| headerMaxWidth    |  Boolean|  false       | 是      |  顶部是否通顶（最大宽度）
| headerCollapseShow|  Boolean|  true        | 是      |  显示收缩菜单
| headerReloadShow  |  Boolean|  true        | 是      |  显示刷新
| headerBreadcrumbShow|Boolean|  true        | 是      |  显示面包屑导航
| headerFullscreenShow|Boolean|  true        | 是      |  显示全屏
| headerNoticeShow  |  Boolean|  true        | 是      |  显示通知
| headerUserShow    |  Boolean|  true        | 是      |  显示用户区域
| headerSettingShow |  Boolean|  true        | 是      |  显示设置
| tabsShow          |  Boolean|  true        | 是      |  显示多页签
| tabsReload        |  Boolean|  true        | 是      |  开启多页签重复点击重载页面
| copyrightShow     |  Boolean|  true        | 是      |  显示底部版权
