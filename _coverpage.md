![logo](_media/logo.svg)

# EggAdminPro <small>1.0.1</small>

> 一个企业级通用后台管理前端中台解决方案

- 简单、轻便 (压缩后 ~21kB)
- 开箱即用
- 众多页面

[comment]: <> ([GitHub基础版]&#40;https://github.com/zengxiaohui2019/eggadminpro/&#41;)
[快速开始](quickstart/README.md)

<!-- 背景色 -->

![color](#f0f0f0)
